*** Settings ***
Documentation   Examples with locators en robotframework
Resource  recursos.robot

*** Test Cases ***
L001 Ejemplo Locator CSS
  [Tags]  now
  Abrir Navegador
  #Posicionarse en el objeto en la pestaña "Elements" del Browser Chrome, hacer click derecho con el mouse y seleccionar copy>> copy selector
  #Page Should Contain Element  css=#buttonwithclass  #esta validando que aparezca un boton, el copy selector me da: #buttonwithclass
  Page Should Contain Element  css:#buttonwithclass
  # cuando se usan los dos puntos en el copy selector?
  Closer Browser

L002 Ejemplo Locator ID
  [Tags]  now2
  Abrir Navegador
  #Posicionarse en el objeto en la pestaña "Elements" del Browser Chrome, hacer click derecho con el mouse y seleccionar Edit as html y copiar id
  Input Text  datepicker  25/12/2019
  # date picker es el id, que no se escribe id, va directamente el id del campo, luego el valor a ingresar
  Close Browser

L003 Ejemplo Locator XPATH
    [Tags]  now3
  Abrir Navegador
  #Posicionarse en el objeto en la pestaña "Elements" del Browser Chrome, hacer click derecho con el mouse y seleccionar copy>> copy xpath
  Page Should Contain Element  //*[@id="profession-1"]
  Close Browser

L004 Ejemplo Locator Link 1
  [Tags]  now4
  Abrir Navegador
  Sleep  5
  Click Element  css=a#cookie_action_close_header
  #Posicionarse en el objeto en la pestaña "Elements" del Browser Chrome, hacer click derecho con el mouse y  seleccionar Edit as html y copiar url
  Click Link  http://toolsqa.com/automation-practice-form/
  Closer Browser

L005 Ejemplo Locator Link 2
    [Tags]  now5
  Abrir Navegador
  sleep  5
  Click Element  css:#cookie_action_close_header
  #Posicionarse en el objeto en la pestaña "Elements" del Browser Chrome, hacer click derecho con el mouse y  seleccionar Edit as html y copiar texto
  Click Link  //a[@href='http://toolsqa.com/automation-practice-form/']
  Close Browser

